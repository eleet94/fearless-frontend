// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/states/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('state');

        for (const state of data.states) {
            const option = document.createElement('option'); // Create an 'option' element
            option.value = state.abbreviation // Set the '.value' property of the option element to the state's abbreviation
            option.innerHTML = state.name // Set the '.innerHTML' property of the option element to the state's name
            selectTag.appendChild(option); // Append the option element as a child of the select tag
        }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(locationUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newLocation = await response.json();
            } else {
                console.log('Error!:', response.status);
            }
        } catch (error) {
            console.log('Error!:', error);
        }

        });
    }
});


// the code needed when the page loads will call that RESTful
// API you just created, get the data back, then loop through it. And for each state in it,
// it'll create an option element that has a value of the abbreviation and the text of the name.
