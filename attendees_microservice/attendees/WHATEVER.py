function median(numbers) {
  const numsCopy = [...numbers];
  numsCopy.sort(function (val1, val2) { return val1 - val2 });
  if (numsCopy.length % 2 === 0) {
    const halfLen = numsCopy.length / 2;
    const [num1, num2] = numsCopy.slice(halfLen - 1, halfLen + 1);
    return (num1 + num2) / 2;
  } else {
    const middle = Math.floor(numsCopy.length / 2);
    return numsCopy[middle];
  }



function mode(numbers) {
   const numCount = {};
   for (const num of numbers) {
      if (numCount[num] === undefined) {
         numCount[num] = 0;
      }1
      numCount[num] += 1;
   }





   let max = null;
   let maxNum = null;
   for (const num in numCount) {
      if (numCount[num] > max) {
         max = numCount[num];
         maxNum = num;
      }
   }
   return maxNum;
}





window.addEventListener('DOMContentLoaded', async () => {
  const url = 'http://localhost:8000/api/conferences/';
  const response = await fetch(url);

  if (response.ok) {
    const data = await response.json();
    const selectTag = document.getElementById('conference');

    for (const conference of data.conferences) {
      const option = document.createElement('option');
      option.value = conference.id;
      option.innerHTML = conference.name;
      selectTag.appendChild(option);
    }

    const loadingSpinner = document.getElementById('loading-conference-spinner');
    const formTag = document.getElementById('create-attendee-form');
    const successMessage = document.getElementById('success-message');

    // Hide the loading spinner and show the conference dropdown

    loadingSpinner.style.display = 'none';
    selectTag.classList.remove('d-none');

    formTag.addEventListener('submit', async (event) => {
      event.preventDefault();

      const formData = new FormData(formTag);
      const json = JSON.stringify(Object.fromEntries(formData));

      const locationUrl = 'http://localhost:8000/api/conferences/';
      const fetchConfig = {
        method: 'post',
        body: json,
        headers: {
          'Content-Type': 'application/json',
        },
      };

      try {
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          formTag.style.display = 'none'; // Hide the form
          successMessage.style.display = 'block'; // Show the success alert
          formTag.reset();
          const newLocation = await response.json();
        } else {
          console.log('Error!:', response.status);
        }
      } catch (error) {
        console.log('Error!:', error);
      }
    });
  }
});
