window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/locations/';
    const response = await fetch(url);

    if (response.ok) {
        const data = await response.json();
        const selectTag = document.getElementById('location');

        for (const location of data.locations) {
            const option = document.createElement('option'); // Create an 'option' element
            option.value = location.id // Set the '.value' property of the option element to the location's abbreviation
            option.innerHTML = location.name // Set the '.innerHTML' property of the option element to the state's name
            selectTag.appendChild(option); // Append the option element as a child of the select tag
        }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json',
            },
        };

        try {
            const response = await fetch(conferenceUrl, fetchConfig);
            if (response.ok) {
                formTag.reset();
                const newConference = await response.json();
            } else {
                console.log('Error!:', response.status);
            }
        } catch (error) {
            console.log('Error!:', error);
        }

        });
    }
});


// the code needed when the page loads will call that RESTful
// API you just created, get the data back, then loop through it. And for each state in it,
// it'll create an option element that has a value of the abbreviation and the text of the name.
