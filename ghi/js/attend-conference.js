// "We need to add an event listener for when the DOM loads."
// "Let's declare a variable that will hold the URL for the API that we just created."
// "Let's fetch the URL. Don't forget the await keyword so that we get the response, not the Promise."
// "If the response is okay, then let's get the data using the .json method. Don't forget to await that, too."

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      const selectTag = document.getElementById('conference');

      for (const conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      const loadingSpinner = document.getElementById('loading-conference-spinner');
      const formTag = document.getElementById('create-attendee-form');
      const successMessage = document.getElementById('success-message');

      loadingSpinner.classList.add('d-none');
      selectTag.classList.remove('d-none');

      formTag.addEventListener('submit', async (event) => {
        event.preventDefault();

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const attendeesUrl = 'http://localhost:8001/api/attendees/';
        const fetchConfig = {
          method: 'post',
          body: json,
          headers: {
            'Content-Type': 'application/json',
          },
        };

        try {
          const response = await fetch(attendeesUrl, fetchConfig);
          if (response.ok) {
            const newAttendee = await response.json();
            formTag.classList.add('d-none');
            successMessage.classList.remove('d-none');
          } else {
            console.log('Error!:', response.status);
          }
        } catch (error) {
          console.log('Error!:', error);
        }
      });
    }
  });

// the code needed when the page loads will call that RESTful
// API you just created, get the data back, then loop through it. And for each state in it,
// it'll create an option element that has a value of the abbreviation and the text of the name.
